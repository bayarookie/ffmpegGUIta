ffmpegGUIta
===========

*GUI for ffmpeg*

Dificult to use. Only for commandliners.

ffmpeg, ffprobe, ffplay 6

https://www.ffmpeg.org/download.html

[Free Pascal 3.2.2 - Lazarus 2.2.6](http://www.lazarus-ide.org/)

![output](./ffmpegGUIta_01.png "Output")

![video](./ffmpegGUIta_02.png "Video")

![audio](./ffmpegGUIta_03.png "Audio")

![settings](./ffmpegGUIta_04.png "Settings")
