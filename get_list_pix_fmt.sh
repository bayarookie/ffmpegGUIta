#!/bin/bash
o=test_list_pix_fmt.pas
ffmpeg -version > $o
ENC=$(ffmpeg -hide_banner -encoders | grep '^[ ][V]' | cut -c 9-29)
echo $ENC >> $o
for v in $ENC; do
  echo $v
  PIX=$(ffmpeg -hide_banner -h "encoder=$v" | grep 'Supported pixel formats:' | cut -c 30-)
  echo "if s = '$v' then" >> $o
  s="  cmbpix_fmt.Items.AddStrings([''"
  for p in $PIX
  do
    s+=", '$p'"
  done
  s+="], True)"
  echo "$s" >> $o
  echo "else" >> $o
done
